# The Shell Mod

Simple theme for [Ghost](http://github.com/tryghost/ghost/).

* Pure CSS
* Web Safe fonts
* Updated to work with Ghost 1+

You can find live example [here](https://blog.sehro.org/)
